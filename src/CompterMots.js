function CompterMots(phrase) {
    let asciiSpace = {'\t': 1, '\n': 1, '\v': 1, '\f': 1, '\r': 1, ' ': 1};
    let nb = 0;
    let i = 0;
    let motStart = 0;
    // Suppression des espaces du début
    while ((i < phrase.length) && (asciiSpace[phrase[i]] != undefined)) {
        i++;
    }
    motStart = i;
    // Je commence à parcourir le mot
    while (i < phrase.length) {
        if (asciiSpace[phrase[i]] == undefined) {
            i++;
            continue;
        }
        nb++;
        i++;
        while ((i < phrase.length) && (asciiSpace[phrase[i]] != undefined)) {
            i++;
        }
        motStart = i;
    }
    if (motStart < phrase.length) {
        nb++;
    }
    return nb;
}
